import { Home, Login } from '@mui/icons-material'
import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

const Router = () => {
  return (
    <>
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Login/>}/>
                <Route path='home' element={<Home/>}/>
            </Routes>
        </BrowserRouter>
    </>
  )
}

export default Router